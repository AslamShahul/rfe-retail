import { Component, ElementRef, ViewChild, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import * as pnp from "sp-pnp-js";
import { ODataDefaultParser } from "sp-pnp-js";
import NodeFetchClient from 'node-pnp-js';
import { AppSettings } from '../Shared/app.settings';
import { Popup } from 'ng2-opd-popup';
//import "~bootstrap/dist/css/bootstrap.min.css";
//import "~font-awesome/css/font-awesome.css";
@Component({
    selector: 'Cancelbutton-app',
    templateUrl: '../Shared/CancelButton.component.html',
    styleUrls: ['../Shared/bootstrap.css']
})
export class CancelButtonComponent implements OnInit {
    registerForm: FormGroup;
    someProperty = true;
    private qUERYSTRINGID;
    userflg= true;
    @Input('name') strname: string = '';
    @Input('myComments') strmyComments: string;
    @ViewChild('popup1') popup1: Popup;
    constructor(private formBuilder: FormBuilder, private popup: Popup) { }
    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            notes: ['', Validators.required]
        });

        this.showhide();        
        //this.showhideCancellbutton();
      
        

    }

    showpopup() {

        this.popup1.options = {
            showButtons: false,
            color: "#4180ab",
            header: "Notes"
        }
        this.popup1.show(this.popup1.options);
    }

    closepopup() {
        this.popup1.hide();

    }
    showhide() {


        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).siteUsers.getById(_spPageContextInfo.userId).groups.filter("Title eq 'RFE Approval Retail Owners'").get().then((userresults: any) => {

            if (userresults.length != 0) {
                this.showhideCancellbutton();
                
                

            }
            else { this.someProperty = false; }

        });
        
    }

    showhideCancellbutton() {

        this.qUERYSTRINGID = getParameterByName1("ID", window.location.href);
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('RFE').items.getById(this.qUERYSTRINGID).select('RFE_x0020_Approval_x0028_1_x0029_','RFE_x0020_Status').get().then((results: any) => {

            if (results.length != 0 &&  ((results.RFE_x0020_Status == "Closed") || results.RFE_x0020_Status == "Cancelled" || !results.RFE_x0020_Approval_x0028_1_x0029_)) {
               
                return this.someProperty = false;

            }
            else { this.someProperty = true; }

        });
    }
}



function getParameterByName1(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
