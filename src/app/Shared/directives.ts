import { Directive, HostBinding, HostListener, ElementRef, Input, EventEmitter, Component } from '@angular/core';
import * as angular from '@angular/core';
import * as ng from '@angular/core';
import * as pnp from "sp-pnp-js";
import { JsomNode } from 'sp-jsom-node';
import { AppSettings } from '../Shared/app.settings';

@Directive({
    selector: '[initiatewf]'
})
export class clickWf {

    constructor(private el: ElementRef) { }
    @HostBinding('value') attrvalue: string;
    @HostBinding('class') attrclass: string;
    @HostBinding('disabled') attrdisabled: string;
    @Input() DefaultVal: string;
    numberOfClicks = 0;

    private qUERYSTRINGID;
    //public click = new EventEmitter();
    @HostListener('click', ['$event.target'])
    onClick(btn) {
        this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        StartWorkflow('1F43F816-5EB6-40A8-A6EE-505C86245A40', this.qUERYSTRINGID)
    }
    private highlight(color: string) {
        this.el.nativeElement.style.backgroundColor = color;
    }
    ngOnInit() {
        this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        this.attrvalue = "Initiate Approval";
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('RFE').items.getById(this.qUERYSTRINGID).select('RFE_x0020_Approval_x0028_1_x0029_', 'RFE_x0020_Status').get().then((results: any) => {
            //this.Tasks=results;
            console.log(results)
            if (results.RFE_x0020_Approval_x0028_1_x0029_ == null) {
                this.attrvalue = "Initiate Approval";
                this.attrclass = "btn btn-info active"

            }
            else if (results.length =! 0 && results.RFE_x0020_Approval_x0028_1_x0029_.Description == "Approval Process Completed" && (results.RFE_x0020_Status != "Cancelled" && results.RFE_x0020_Status != "Closed")) {
                this.attrvalue = "Approval process completed";
                this.attrclass = "btn btn-info disabled"
                this.attrdisabled = "disabled"
            }
            else if (results.length =! 0 && results.RFE_x0020_Approval_x0028_1_x0029_.Description != "Approval Process Completed" && (results.RFE_x0020_Status != "Cancelled" && results.RFE_x0020_Status != "Closed")) {
                this.attrvalue = "Approval In Progress";
                this.attrclass = "btn btn-info disabled"
                this.attrdisabled = "disabled"
            }
            else if (results.RFE_x0020_Status == "Cancelled") {
                this.attrvalue = "Approval Cancelled";
                this.attrclass = "btn btn-info disabled"
                this.attrdisabled = "disabled"
            }
            else if (results.RFE_x0020_Status == "Closed") {
                this.attrvalue = "Approval Closed";
                this.attrclass = "btn btn-info disabled"
                this.attrdisabled = "disabled"
            }

        });
    }
}

var dlg = null;
//Subscription id - Workflow subscription id
//list item id for which to start workflow. If site workflow, then send null for itemId
function StartWorkflow(subscriptionId, itemId) {
    showInProgressDialog();
    ExecuteOrDelayUntilScriptLoaded(function () {
        ExecuteOrDelayUntilScriptLoaded(function () {
            SP.SOD.registerSod('SP.ClientContext', SP.Utilities.Utility.getLayoutsPageUrl('sp.js'));
            SP.SOD.registerSod('SP.WorkflowServices.WorkflowServicesManager', SP.Utilities.Utility.getLayoutsPageUrl('SP.WorkflowServices.js'));
            SP.SOD.loadMultiple(['SP.ClientContext', 'SP.WorkflowServices.WorkflowServicesManager'], function () {
                var ctx = SP.ClientContext.get_current();

                var wfManager = SP.WorkflowServices.WorkflowServicesManager.newObject(ctx, ctx.get_web());
                var subscription = wfManager.getWorkflowSubscriptionService().getSubscription(subscriptionId);
                console.log(subscriptionId)
                console.log(subscription)
                ctx.load(subscription, 'PropertyDefinitions');
                ctx.executeQueryAsync(
                    function (sender, args) {
                        var params = new Object();
                        //Find initiation data to be passed to workflow.
                        var formData = subscription.get_propertyDefinitions()["FormData"];
                        if (formData != null && formData != 'undefined' && formData != "") {
                            var assocParams = formData.split(";#");
                            for (var i = 0; i < assocParams.length; i++) {
                                params[assocParams[i]] = subscription.get_propertyDefinitions()[assocParams[i]];
                            }
                        }
                        if (itemId) {

                            wfManager.getWorkflowInstanceService().startWorkflowOnListItem(subscription, itemId, params);
                        }
                        else {
                            wfManager.getWorkflowInstanceService().startWorkflow(subscription, params);
                        }
                        ctx.executeQueryAsync(
                            function (sender, args) {


                                closeInProgressDialog();
                                alert("Approval Workflow has been initiated successfully");
                                this.attrvalue = "Approval In Progress";
                                this.attrclass = "btn btn-info disabled"

                            },
                            function (sender, args) {
                                alert('Failed to run workflow');
                                closeInProgressDialog();
                                alert('Failed to run workflow');
                            }
                        );
                    },
                    function (sender, args) {
                        alert('Failed to run workflow');
                        closeInProgressDialog();
                        alert('Failed to run workflow');
                    }
                );
            });
        }, "sp.js");
    }, "sp.runtime.js");
    //const ctx = SP.ClientContext.get_current();


}
function closeInProgressDialog() {
    if (dlg != null) {
        dlg.close();
    }
}


function showInProgressDialog() {
    if (dlg == null) {
        dlg = SP.UI.ModalDialog.showWaitScreenWithNoClose("Please wait...", "Waiting for workflow...", null, null);
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}



function terminateWorkflow(listId, itemId, subId) {
    ExecuteOrDelayUntilScriptLoaded(function () {
        ExecuteOrDelayUntilScriptLoaded(function () {
            SP.SOD.registerSod('SP.ClientContext', SP.Utilities.Utility.getLayoutsPageUrl('sp.js'));
            SP.SOD.registerSod('SP.WorkflowServices.WorkflowServicesManager', SP.Utilities.Utility.getLayoutsPageUrl('SP.WorkflowServices.js'));
            SP.SOD.loadMultiple(['SP.ClientContext', 'SP.WorkflowServices.WorkflowServicesManager'], function () {
                var context = SP.ClientContext.get_current();
                var workflowServicesManager = SP.WorkflowServices.WorkflowServicesManager.newObject(context, context.get_web());
                var workflowInstanceService = workflowServicesManager.getWorkflowInstanceService();
                var wfInstances = workflowInstanceService.enumerateInstancesForListItem(listId, itemId);
                context.load(wfInstances);
                context.executeQueryAsync(
                    function (sender, args) {
                        var instancesEnum = wfInstances.getEnumerator();
                        while (instancesEnum.moveNext()) {
                            var instance = instancesEnum.get_current();
                            if (instance.get_workflowSubscriptionId().toString() == subId) {
                                workflowInstanceService.terminateWorkflow(instance);
                                context.executeQueryAsync(
                                    function (sender, args) {
                                        console.log("Termination Successful");
                                    },
                                    function (sender, args) {
                                        console.log("Failed to terminate workflow.");
                                        console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
                                    }
                                );
                            }
                        }
                    },
                    function (sender, args) {
                        console.log("Failed to load instances.");
                        console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
                    }
                );
            });
        }, "sp.js");
    }, "sp.runtime.js");
}

//dialog element to show during processing
