import { Directive, HostBinding, HostListener, ElementRef, Input, EventEmitter, Component } from '@angular/core';
import * as angular from '@angular/core';
import * as ng from '@angular/core';
import * as pnp from "sp-pnp-js";
import { JsomNode } from 'sp-jsom-node';
import { AppSettings } from '../Shared/app.settings';

@Directive({
    selector: '[Cancelwf]'
})
export class CancelWf {

    constructor(private el: ElementRef) { }
    @HostBinding('value') attrvalue: string;
    @HostBinding('class') attrclass: string;
    @HostBinding('disabled') attrdisabled: string;
    @HostBinding('style.display') attrdisplay: string;
    @Input() DefaultVal: string;
    @Input('Comments') Comments;
    numberOfClicks = 0;

    private qUERYSTRINGID;
    private currentUser;
    private UserID;
    private oWeb;
    private id: number;
    private status: string;
    private userflag: boolean;
    private flgforcancelbutton: boolean;
    //Comments='';
    //public click = new EventEmitter();
    @HostListener('click', ['$event.target'])
    onClick(btn) {       
        this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        this.Getusergroup(this.qUERYSTRINGID, this.status, this.Comments);
    }
    private highlight(color: string) {
        this.el.nativeElement.style.backgroundColor = color;
    }
    ngOnInit() {
        this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        this.attrvalue = "Initiate Approval";
        this.flgforcancelbutton = false;
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).siteUsers.getById(_spPageContextInfo.userId).groups.filter("Title eq 'RFE Approval Retail Owners'").get().then((results: any) => {

            if (results.length != 0) {
                console.log(2);
                this.attrdisplay = "inline";

            }
            else { this.attrdisplay = "none" }

        });

        //if(GetusergroupforshowingCancelbutton()){console.log (this.flgforcancelbutton);this.attrdisplay="none"};
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('RFE').items.getById(this.qUERYSTRINGID).select('RFE_x0020_Approval_x0028_1_x0029_', 'RFE_x0020_Status').get().then((results: any) => {
            //this.Tasks=results;
            console.log(results)

            if (results.RFE_x0020_Approval_x0028_1_x0029_ == null || results.RFE_x0020_Status == "Cancelled") {
                this.attrvalue = "Cancel RFE";
                this.attrclass = "btn btn-info disabled";
                this.attrdisabled = "disabled";
                console.log("TEst1");

            }
            else if (results.length != 0 && results.RFE_x0020_Approval_x0028_1_x0029_.Description == "Approval Process Completed" && results.RFE_x0020_Status == "In Progress") {
                this.attrvalue = "Close RFE";
                this.attrclass = "btn btn-info active";
                this.status = "Closed";
                console.log("TEst2");

            }
            else if (results.length != 0 && results.RFE_x0020_Approval_x0028_1_x0029_.Description == "Approval Process Completed" && results.RFE_x0020_Status == "Closed") {
                this.attrvalue = "Close RFE";
                this.attrclass = "btn btn-info disabled";
                this.attrdisabled = "disabled";
                console.log("TEst3");

            }

            else if (results.length != 0 && results.RFE_x0020_Approval_x0028_1_x0029_.Description != "Approval Process Completed" && results.RFE_x0020_Status == "In Progress") {
                this.attrvalue = "Cancel RFE";
                this.attrclass = "btn btn-info active";
                this.status = "Cancelled";
                console.log("TEst4");

            }

        });
    }
    Getusergroup(ID, status, Comments) {
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).siteUsers.getById(_spPageContextInfo.userId).groups.filter("Title eq 'RFE Approval Retail Owners'").get().then(results => {

            if (results.length != 0) {

                AddStatus(ID, status, Comments);
                if (status == "Cancelled") {
                    terminateWorkflow('245AD936-0A7D-461E-8535-4FF2DEDF3803', ID, '1F43F816-5EB6-40A8-A6EE-505C86245A40')
                    alert("RFE is cancelled");
                }
                else{
                        alert("RFE has been closed");
                }
                



            }
            else { alert("Access Denied");return; }

        });
        
        this.attrclass = "btn btn-info disabled";
        this.attrdisabled = "disabled";
        window.close();
    }
}

var dlg = null;
//Subscription id - Workflow subscription id
//list item id for which to start workflow. If site workflow, then send null for itemId
function terminateWorkflow(listId, itemId, subId) {

    ExecuteOrDelayUntilScriptLoaded(function () {
        ExecuteOrDelayUntilScriptLoaded(function () {
            SP.SOD.registerSod('SP.ClientContext', SP.Utilities.Utility.getLayoutsPageUrl('sp.js'));
            SP.SOD.registerSod('SP.WorkflowServices.WorkflowServicesManager', SP.Utilities.Utility.getLayoutsPageUrl('SP.WorkflowServices.js'));
            SP.SOD.loadMultiple(['SP.ClientContext', 'SP.WorkflowServices.WorkflowServicesManager'], function () {
                var context = SP.ClientContext.get_current();

                var workflowServicesManager = SP.WorkflowServices.WorkflowServicesManager.newObject(context, context.get_web());
                var workflowInstanceService = workflowServicesManager.getWorkflowInstanceService();


                var wfInstances = workflowInstanceService.enumerateInstancesForListItem(listId, itemId);

                context.load(wfInstances);
                context.executeQueryAsync(
                    function (sender, args) {
                        var instancesEnum = wfInstances.getEnumerator();
                        while (instancesEnum.moveNext()) {

                            var instance = instancesEnum.get_current();

                            if (instance.get_workflowSubscriptionId().toString().toUpperCase() == subId) {
                                workflowInstanceService.cancelWorkflow(instance);
                                context.executeQueryAsync(
                                    function (sender, args) {
                                        console.log("Termination Successful");
                                        
                                    },
                                    function (sender, args) {
                                        console.log("Failed to terminate workflow.");
                                        console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
                                    }
                                );
                            }
                        }
                    },
                    function (sender, args) {
                        console.log("Failed to load instances.");
                        console.log("Error: " + args.get_message() + "\n" + args.get_stackTrace());
                    }
                );
            });
        }, "sp.js");
    }, "sp.runtime.js");
}

function closeInProgressDialog() {
    if (dlg != null) {
        dlg.close();
    }
}


function showInProgressDialog() {
    if (dlg == null) {
        dlg = SP.UI.ModalDialog.showWaitScreenWithNoClose("Please wait...", "Waiting for workflow...", null, null);
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function AddStatus(ID, Status, Comments) {

    new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('RFE').items.getById(ID).update({ "RFE_x0020_Status": Status, "Reason_x0020_for_x0020_Cancellation": Comments }).then(r => {
    //alert("The RFE Status is set to "+Status);
    
    }).catch(function (err) {
        alert("Access Denied.Please contact Finance team")
        return;
    });

    
    
}

/*function Getusergroup(ID, status, Comments) {
    new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).siteUsers.getById(_spPageContextInfo.userId).groups.filter("Title eq 'RFE Approval Owners'").get().then(results => {

        if (results.length != 0) {

            AddStatus(ID, status, Comments);
            if (status == "Cancelled") {
                terminateWorkflow('14130EDC-54BE-4364-8A7E-858AC45D12DE', ID, '18516CDB-4112-4381-B509-FC15B97EA6AB')
                 
            }


        }
        else { alert("Access Denied");return; }

    });
    this.attrclass = "btn btn-info disabled";
    this.attrdisabled = "disabled";
}*/

function GetusergroupforshowingCancelbutton() {
    console.log(1);
    new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).siteUsers.getById(_spPageContextInfo.userId).groups.filter("Title eq 'RFE Approval Owners'").get().then((results: any) => {

        if (results.length != 0) {
            console.log(2);
            this.attrdisplay = "none";

        }
        else { this.attrdisplay = "inline" }

    })
}

//B9F5FF17-E37C-40F1-A80C-3070D1FB1362

//dialog element to show during processing
