import {Component, EventEmitter, Input, Output} from '@angular/core';
@Component({
    selector: 'text-app',
    templateUrl: '../Shared/textbox.component.html'
})
export class TextBoxComponent {
	
    @Input('myComments') strmyComments : string;
  	@Output('myCommentsChange') myCommentsObj = new EventEmitter<string>();
	emitComments() {
	    this.myCommentsObj.emit(this.strmyComments);
    }
}   