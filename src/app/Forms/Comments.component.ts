import { Component, ElementRef, ViewChild, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as pnp from "sp-pnp-js";
import { ODataDefaultParser } from "sp-pnp-js";
import NodeFetchClient from 'node-pnp-js';
import { AppSettings } from '../Shared/app.settings';
import { Popup } from 'ng2-opd-popup';
import { IResult } from '../Shared/Notes.entities';

@Component({
    selector: 'NotesForm',
    templateUrl: '../Forms/Comments.component.html',
    styleUrls: ['./Comments.component.css', '../Shared/bootstrap.css']

})


export class CommentsComponent implements OnInit {
    registerForm: FormGroup;
    private qUERYSTRINGID;
    private AllNotes: IResult[] = [];
    private ChildVal: IResult[] = [];
    private NewNotes="#666666";
    @ViewChild('popup1') popup1: Popup;
    @ViewChild('popup2') popup2: Popup;
    @Input('myComments') strmyComments: string;
    displaystyle = "none";
    comtsviewbuttonstyle="inline";
    constructor(private formBuilder: FormBuilder, private popup: Popup) { }


    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            notes: ['', Validators.required]
        });
        this.GetNotes();

    }
    AddComments(notes) {
        var RFEID = getParameterByName("ID", window.location.href);
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Comments').items.add({ "Comments": notes.notes, "RFE_x0020_ID": RFEID }).then(r => {
            alert("Comments added Successfully");
            this.popup.hide();
        });

    }
    showpopup() {
        this.popup2.hide();
        this.popup1.options = {
            showButtons: false,
            color: "#4180ab",
            header: "Notes"
        }
        this.popup1.show(this.popup1.options);
    }
    closepopup() {
        this.popup1.hide();
        this.popup2.hide();
    }
    showDetilpopup() {
        this.GetNotes();
        this.popup2.show(this.popup2.options);



    }
    AddReplybox(i) {

        for (let Notesresult of this.AllNotes) {

            if (Notesresult.ID == i) {
                Notesresult.Display = "block"

            }
        }
    }

    UpdateNotes(strmyComments, ParentID, ID) {
        var RFEID = getParameterByName("ID", window.location.href);
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Comments').items.add({ "Comments": strmyComments, "RFE_x0020_ID": RFEID, "ParentId": ID }).then(r => {
            alert("Comments added Successfully");
            this.popup.hide();
        });
    }

    GetNotes() {
        this.popup2.hide();
        console.log(1)
        try {
            let params = (new URL(window.location.href)).searchParams;
            this.qUERYSTRINGID = params.get("ID");
        }
        catch (Error) {
            console.log(Error);
            this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        }

        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Comments').items.select('Comments', 'Created', 'Author/Id', 'Author/Title', 'ParentId', 'ID', 'Display', 'LeftMargin').filter("RFE_x0020_ID eq '" + this.qUERYSTRINGID + "'").expand('Author').orderBy("ID").orderBy("ParentId").get().then((results: any[]) => {
            console.log(2);
            console.log(results);
            if(results.length==0){
                
                 this.NewNotes="#ba122b"
                  this.comtsviewbuttonstyle="none";
            }
           
            var q = new Date();
            var m = q.getMonth();
            var d = q.getDate();
            var y = q.getFullYear();
            let date = new Date(y,m,d);
            this.AllNotes = []
            
            for (let Notesresult of results) {
                console.log(Notesresult.ParentId);
                let newDate = new Date(Notesresult.Created);
                if(date.valueOf() - newDate.valueOf() < 2){

                    this.NewNotes="#45edd4";
                    
                }
                if (!Notesresult.ParentId) {
                    console.log("Success");
                    Notesresult.Display = "none";
                    Notesresult.LeftMargin = "1cm"
                    this.AllNotes.push(Notesresult);
                    this.ChildVal = results.filter(results => results.ParentId === Notesresult.ID)
                    for (let filteredresult of this.ChildVal) {
                        filteredresult.Display = "none";
                        filteredresult.LeftMargin = "2cm"
                        this.AllNotes.push(filteredresult);
                    }
                }


            }
            console.log(3)
            console.log(this.AllNotes)
            this.popup2.options = {
                showButtons: false,
                color: "#4180ab",
                header: "Notes Detail", cancleBtnContent: "Cancel", cancleBtnClass: "btn btn-default"
            }
            //this.popup2.show(this.popup2.options);

        });

    }
}




function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


