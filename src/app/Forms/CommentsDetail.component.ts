import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TextBoxComponent } from '../Shared/textbox.component';
import { ButtonComponent } from '../Shared/Button.component';
import { InitiateWorkflowComponent } from '../InitiateWorkflow/InitiateWorkflow.component';
import { AppSettings } from '../Shared/app.settings';
import { IResult } from '../Shared/app.entities';
import * as pnp from "sp-pnp-js";
import { ODataDefaultParser } from "sp-pnp-js";
import NodeFetchClient from 'node-pnp-js';

@Component({
    selector: 'NotesDetail',
    templateUrl: './CommentsDetail.component.html',

})
export class CommentsDetail implements OnInit {
    private qUERYSTRINGID;
    private AllNotes: IResult[] = [];


    ngOnInit() {

        pnp.setup({

            headers: {
                "Accept": "application/json;odata=verbose",
            }
        });
        this.GetNotes();
    }

    GetNotes() {
        console.log(1)
        try {
            let params = (new URL(window.location.href)).searchParams;
            this.qUERYSTRINGID = params.get("ID");
        }
        catch (Error) {
            console.log(Error);
            this.qUERYSTRINGID = getParameterByName("ID", window.location.href);
        }




        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Comments').items.select('Comments', 'Created', 'Author/Id', 'Author/Title').filter("RFE_x0020_ID eq '" + this.qUERYSTRINGID + "'").expand('Author').get().then((results: any) => {
            var Notes = results

        });

    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}