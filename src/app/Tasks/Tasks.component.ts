import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TextBoxComponent } from '../Shared/textbox.component';
import { ButtonComponent } from '../Shared/Button.component';
import { InitiateWorkflowComponent } from '../InitiateWorkflow/InitiateWorkflow.component';
import { AppSettings } from '../Shared/app.settings';
import { IResult } from '../Shared/app.entities';
import * as pnp from "sp-pnp-js";
import { ODataDefaultParser } from "sp-pnp-js";
import NodeFetchClient from 'node-pnp-js';



@Component({
    selector: 'app-test',
    templateUrl: './Tasks.component.html',
    styleUrls: ['./Tasks.component.css']
})


export class TasksComponent implements OnInit {

    private InprogressTasks: IResult[] = [];
    private AllTasks: IResult[] = [];
    Comments = '';
    private qUERYSTRINGID;



    ngOnInit() {

        pnp.setup({

            headers: {
                "Accept": "application/json;odata=verbose",
            }
        });
        this.GetTasks();
    }

    GetTasks() {
        console.log(1)
        try {
            let params = (new URL(window.location.href)).searchParams;
            this.qUERYSTRINGID = params.get("ID");
        } catch (Error) {
            console.log(Error);
            this.qUERYSTRINGID = this.getParameterByName("ID", window.location.href);
        }



        console.log(2)
        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).currentUser.select('ID').get(new ODataDefaultParser()).then((result: any) => {
            var Id = result.Id;
            new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Workflow Tasks').items.select('Id', 'Title', 'RelatedItems', 'AssignedTo/Id', 'AssignedTo/Title', 'TaskOutcome', 'Status', 'Comments','Modified').filter("substringof('" + this.qUERYSTRINGID + "',Title)").expand('AssignedTo').get().then((results: any) => {
                //this.Tasks=results;
                for (let taskresult of results) {

                    if (taskresult.RelatedItems.split(",")[0].split(":")[1] == this.qUERYSTRINGID && taskresult.Status != "Completed" && taskresult.AssignedTo.Id == Id) {

                        this.InprogressTasks.push(taskresult);

                    }
                    if (taskresult.RelatedItems.split(",")[0].split(":")[1] == this.qUERYSTRINGID) {
                        this.AllTasks.push(taskresult);

                    }

                }

            });
        });
    }

    TaskAction(TaskID, outcome) {

        if (outcome == 'Rejected' && this.Comments.length == 0) {
            alert('Please enter the reason for rejection in the Comments box');
            return false;
        }

        new pnp.Web(AppSettings.SHAREPOINT_SITE_URL).lists.getByTitle('Workflow Tasks').items.getById(TaskID).update({ "TaskOutcome": outcome, "Status": "Completed", "PercentComplete": 1, "Comments": this.Comments }).then(r => {
            alert(outcome);
            this.InprogressTasks = []
            this.AllTasks = []
            this.GetTasks()
        });

    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

}